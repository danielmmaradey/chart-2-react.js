export default 
  [
      {
        name: 'Dashboard',
        url: '/',
        icon: 'fa fa-bar-chart',
        classNam: 'a-dashboard'
      },
      {
        name: 'Products',
        url: '/products',
        icon: 'fa fa-cart-arrow-down',
        classNam: 'a-products'
      },
      {
        name: 'Users',
        url: '/users',
        icon: 'fa fa-user',
        classNam: 'a-users'
      },
      {
        name: 'Help',
        url: '/help',
        icon: 'fa fa-cog',
        classNam: 'a-general'
      },
      {
        name: 'Config',
        url: '/config',
        icon: 'fa fa-th-large',
        classNam: 'a-config'
      },
    ];

  