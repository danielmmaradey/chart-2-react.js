//DEPENDENECIES
import React, { Component } from 'react';
//COMPONENT
import Chart2 from '../chart/chart2';
class Exchange extends Component {
    render() {
        return (
            <div className="exchange">
                <div className="sub-title">
                    <i className="fa fa-bookmark"></i>
                    <h1>Lorem Ipsum is simply</h1>
                </div>
                <div className="content">
                    <div className="content-1">
                        <table style={{ width: '100%' }}>
                            <thead>
                                <tr>
                                    <th>Lorem Ipsum</th>
                                    <th>Lorem Ipsum</th>
                                    <th>Lorem</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Jill</td>
                                    <td>Smith</td>
                                    <td>50</td>
                                </tr>
                                <tr>
                                    <td>Eve</td>
                                    <td>Jackson</td>
                                    <td>94</td>
                                </tr>
                                <tr>
                                    <td>John</td>
                                    <td>Doe</td>
                                    <td>74</td>
                                </tr>
                                <tr>
                                    <td>Eddy</td>
                                    <td>Darry</td>
                                    <td>80</td>
                                </tr>
                                <tr>
                                    <td>Miky</td>
                                    <td>Clark</td>
                                    <td>60</td>
                                </tr>
                                <tr>
                                    <td>Josh</td>
                                    <td>Morry</td>
                                    <td>44</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div className="content-2">
                        <Chart2 />
                    </div>
                </div>
            </div>
        );
    }
}

export default Exchange;