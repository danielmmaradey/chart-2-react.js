//DEPENDENCIES
import React, { Component } from 'react';
import { Line } from 'react-chartjs-2';

class Chart2 extends Component{
    state = {
        data: {
            labels: ["1", "2", "3", "4", "5"],
            datasets: [
                {
                    label: "Lorem",
                    backgroundColor: "#304894",
                    data: [5,4,1,10,32,2,12]
                },
                {
                    label: "Lorem2",
                    backgroundColor: "#FC7242",
                    data: [14,15,21,0,12,4,2]
                },
            ]
        }
    }
    render(){
        return(
            <div>
                <Line 
                    height={230}
                    data={this.state.data}
                    options={{
                        responsive: true,
                        scales: {
                            xAxes: [{
                                barThickness: 8.5,
                                ticks: {
                                    fontColor: '#696969',
                                    fontSize: '10',
                                },
                                gridLines: {
                                    display: false,
                                }
                            }],
                            yAxes: [{
                                ticks: {
                                    display: false,
                                    stepSize: 6
                                },
                                scaleLabel: {
                                    display: false,
                                },
                                gridLines: {
                                    color: "#3d3d3d",
                                    borderDash: [0.5, 15],
                                    drawBorder: false,
                                }
                            }]
                        },
                        legend: {
                            display: false
                        }
                    }}
                />
            </div>
        );
    }
}
export default Chart2;