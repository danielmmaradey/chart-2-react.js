import React, { Component } from 'react';
import { Bar } from 'react-chartjs-2';

class Chart extends Component {
    state = {
        chartData: {
            scaleLineColor: "rgba(0,0,0,0.0)",
            labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul'],
            datasets: [
                {
                    stack: 'stack1',
                    label: 'data1',
                    data: [1, 2, 3, 4, 5, 6, 7],
                    backgroundColor: ['#FC7242','#FC7242','#FC7242','#FC7242','#FC7242','#FC7242','#FC7242']
                },
                {
                    stack: 'stack2',
                    label: 'data2',
                    data: [5, 4, 3, 2, 4, 5, 6],
                    backgroundColor: ['#C74855','#C74855','#C74855','#C74855','#C74855','#C74855','#C74855'
                    ]
                },
                {
                    stack: 'stack1',
                    label: 'data2',
                    data: [8, 6, 3, 8, 3, 3, 4],
                    backgroundColor: ['#6FD1F6','#6FD1F6','#6FD1F6','#6FD1F6','#6FD1F6','#6FD1F6','#6FD1F6'
                    ]
                },
                {
                    stack: 'stack1',
                    label: 'data2',
                    data: [5, 4, 3, 2, 6, 5, 6],
                    backgroundColor: ['#304894','#304894','#304894','#304894','#304894','#304894','#304894'
                    ]
                }
            ]
        }
    }
    render() {
        return (
            <div className="chart">
                <Bar data={this.state.chartData}
                    height={110}
                    options={{
                        scales: {
                            xAxes: [{
                                barThickness: 8.5,
                                ticks: {
                                    fontColor: '#696969',
                                    fontSize: '10',
                                },
                                gridLines: {
                                    display: false,
                                }
                            }],
                            yAxes: [{
                                ticks: {
                                    display: false,
                                    stepSize: 6
                                },
                                scaleLabel: {
                                    display: false,
                                },
                                gridLines: {
                                    color: "#3d3d3d",
                                    borderDash: [0.5, 15],
                                    drawBorder: false,
                                }
                            }]
                        },
                        legend: {
                            display: false
                        },
                    }} />

            </div>
        );
    }
}

export default Chart;