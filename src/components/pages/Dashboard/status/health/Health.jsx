import React, { Component } from 'react';

class Health extends Component{
    render(){
        return(
            <div className="health">
                <div className="sub-title">
                    <i className="fa fa-bookmark"></i>
                    <h1>Lorem Ipsum is simply</h1>
                </div>
                <div className="content-health">
                    <div className="box active">
                        <h1>Status</h1>
                        <h2><i className="fa fa-circle" aria-hidden="true"></i> Active</h2>
                    </div>
                    <div className="box">
                        <h1>Lorem Ipsum is simply</h1>
                        <h2>false</h2>
                    </div>
                    <div className="box">
                        <h1>Lorem Ipsum is simply</h1>
                        <h2>3</h2>
                    </div>
                    <div className="box">
                        <h1>Lorem Ipsum is simply</h1>
                        <h2>3</h2>
                    </div>
                    <div className="box">
                        <h1>Lorem Ipsum is simply</h1>
                        <h2>122</h2>
                    </div>
                    <div className="box">
                        <h1>Lorem Ipsum is simply</h1>
                        <h2>244</h2>
                    </div>
                </div>
            </div>
        );
    }
} 

export default Health;