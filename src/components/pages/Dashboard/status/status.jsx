import React, { Component } from 'react';

class Status extends Component {
    render() {
        return (
            <div className="status">
                <div className="title">
                    <h1>Status</h1>
                </div>
                <div className="sub-title">
                    <i className="fa fa-bookmark"></i>
                    <h1>Lorem Ipsum is simply</h1>
                </div>
                <div className="content-status">
                    <div className="box">
                        <div className="content-box">
                            <h1>44,195,042</h1>
                            <i className="fa fa-circle" aria-hidden="true" style={{color: '#C74855'}}></i>
                        </div>
                        <h2>Lorem Ipsum is simply</h2>
                    </div>
                    <div className="box">
                        <div className="content-box">
                            <h1>28,6</h1>
                            <i className="fa fa-circle" aria-hidden="true" style={{color: '#FC7242'}}></i>
                        </div>
                        <h2>Lorem Ipsum is simply</h2>
                    </div>
                    <div className="box">
                        <div className="content-box">
                            <h1>57.2</h1>
                            <i className="fa fa-circle" aria-hidden="true" style={{color: '#304894'}}></i>
                        </div>
                        <h2>Lorem Ipsum is simply</h2>
                    </div>
                    <div className="box">
                        <div className="content-box">
                            <h1>4</h1>
                            <i className="fa fa-circle" aria-hidden="true" style={{color: '#6FD1F6'}}></i>
                        </div>
                        <h2>Lorem Ipsum is simply</h2>
                    </div>
                    <div className="box">
                        <div className="content-box">
                            <h1>420</h1>
                            <i className="fa fa-circle" aria-hidden="true" style={{color: '#C74855'}}></i>
                        </div>
                        <h2>Lorem Ipsum is simply</h2>
                    </div>
                    <div className="box">
                        <div className="content-box">
                            <h1>43</h1>
                            <i className="fa fa-circle" aria-hidden="true" style={{color: '#FC7242'}}></i>
                        </div>
                        <h2>Lorem Ipsum is simply</h2>
                    </div>
                </div>
            </div>
        );
    }
}

export default Status;