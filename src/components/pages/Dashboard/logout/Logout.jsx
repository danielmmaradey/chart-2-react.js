//DEPENDENCIES
import React, { Component } from 'react';

class Logout extends Component {

    render() {
        const { user } = this.props;
        const img = user === null? "" : user;
        console.log(img)
        return (
            <div className="logout">
                <div className="user">
                    <img src={img} alt="" />
                </div>
                <div className="search">
                    <i className="fa fa-search"></i>
                </div>
                <div className="menu">
                    <i className="fa fa-bars"></i>
                </div>
            </div>
        );
    }
}

export default Logout;