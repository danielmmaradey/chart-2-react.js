//DEPENDENCIES
import React, { Component } from 'react';
import axios from 'axios';

//COMPONENTS
import Chart from './chart/chart';
import Average from './average/Average';
import Logout from './logout/Logout';
import Status from './status/status';
import Health from './status/health/Health';
import Exchange from './exchange/exchange';

//SCSS
import './Dashboard.scss';

class Dashboard extends Component {

    state = {
        user: [],
        image: ""
    }
    
    componentDidMount = () =>{
        this.handleGetData();
    }

    handleGetData = () =>{
        const url = 'https://randomuser.me/api/?results=1';
        axios.get(url)
        .then(res => {this.setState({user: res.data.results})})
        .then(res => this.handleLogout())
    }

    handleLogout = () =>{
        console.log(this.state.user[0].picture.medium);
        this.setState({
            image: this.state.user[0].picture.medium
        })
    }

    render() {
        const { image } = this.state;
        return (
            <div className="dashboard">
                <div className="content-dashboard">
                    <div className="content1">
                        <Average />
                        <Chart />
                        <Exchange />
                    </div>
                    <div className="content2">
                        <Logout user={image}/>
                        <Status />
                        <Health />
                    </div>
                </div>

            </div>
        );
    }
}

export default Dashboard;