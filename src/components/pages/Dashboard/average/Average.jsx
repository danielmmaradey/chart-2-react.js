import React, { Component } from 'react';
class Average extends Component {
    render() {
        return (
            <div className="average">
                <div className="content1-a">
                    <div className="sub-content1-a">
                        <h1>Average</h1>
                    </div>
                    <div className="sub-content2-a">
                        <h1>$30,555<b>.67</b></h1>
                        <h2>+ 556.4</h2>
                    </div>
                    <div className="sub-content3-a">
                        <h3>Total average in</h3>
                        <div className="combobox-1">
                            <select>
                                <option value="volvo">Volvo</option>
                                <option value="saab">Saab</option>
                                <option value="opel">Opel</option>
                                <option value="audi">Audi</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div className="content2-a">
                    <div className="combobox-2">
                        <select>
                            <option value="volvo">Volvo</option>
                            <option value="saab">Saab</option>
                            <option value="opel">Opel</option>
                            <option value="audi">Audi</option>
                        </select>
                    </div>
                </div>
            </div>
        );
    }
}

export default Average;