import React, { Component } from 'react';

class Page404 extends Component{
    render(){
        return(
            <div>
                <h1>
                    Page404
                </h1>
                <div className="personal-info">
                    <h1>Daniel Alejandro Mendez Maradey</h1>
                    <h2>damaradey@gmail.com</h2>
                </div>
            </div>
        );
    }
}

export default Page404;