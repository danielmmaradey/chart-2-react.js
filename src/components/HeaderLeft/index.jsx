//Dependencies
import React, { Component } from 'react';
import propTypes from 'prop-types';
import { Link } from 'react-router-dom';

//Components
import Logo from './Logo';

//SCSS
import './Header.scss';

class HeaderLeft extends Component {

    static propTypes = {
        items: propTypes.array.isRequired,
        path: propTypes.string.isRequired
    };

    

    render() {
        const { items, path } = this.props;
        return (
            <div className="header">
                <div className="logo">
                    <Logo />
                </div>
                <div className="sidebar">
                <ul className="Menu">
                    {
                        items && items.map(
                            (item, key) => <li className={`side-menu  ${item.url === path ? "active" : ""} `} key={key} >
                                                <Link className="link" to={item.url}>
                                                    <i className={item.icon}></i>
                                                    <span className="tooltiptext">{item.name}</span>
                                                </Link>    
                                            </li>
                        )
                    }
                </ul>
                </div>

            </div>
        );
    }
}

export default HeaderLeft;