import React from 'react';
import { render } from 'react-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';
import {BrowserRouter as Router } from 'react-router-dom';

import AppRouter from './routes';

render(
    <Router>
        <AppRouter />
    </Router>,  
    document.getElementById('root'));

serviceWorker.unregister();
